#include <linux/module.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/mm.h>
#include <linux/sched.h>
#include <linux/init.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/proc_fs.h>
#include <linux/string.h>
#include <linux/seq_file.h>
#include <linux/uaccess.h>
 
#include <asm/io.h>
#include <asm/switch_to.h>

#define CHRMEM_SIZE 0x400
#define MEM_CLEAR 0x1

static int chr_major;
struct chr_dev
{
    struct cdev cdev;
    unsigned char mem[CHRMEM_SIZE];
    unsigned int nums[CHRMEM_SIZE];
};
static int num_pos;

struct chr_dev* char_devp;

static struct proc_dir_entry* entry;




int isnum(char c){
    return (c >= '0' && c <= '9');
}

int calnum(const char* s, int n){
	int res = 0;
	int num = 0;
	int i;
	for(i =0; i < n; ++i){
		if(i < n && isnum(s[i])){
			while(i<n && isnum(s[i])){
				num = num*10 + (int)(s[i]-'0');
				++i;
			}
			res += num;
			num = 0;
			--i;
		}
	}
	return res;
}

int chr_open(struct inode* inode, struct file* filp){
    filp->private_data = char_devp;
    return 0;
}

int chr_release(struct inode* inode, struct file* filp){
    return 0;
}


static ssize_t chr_read(struct file* filp, char __user* buf, size_t size, loff_t* ppos){
    unsigned long p = *ppos;
    unsigned int count = size;
    int ret = 0;
    struct chr_dev* dev = filp->private_data;
    char tbuf[CHRMEM_SIZE];
    int index;
    int len = 0;
    memset(tbuf,0,CHRMEM_SIZE);
    for(index=0;index<num_pos;++index){
		len+=sprintf(tbuf+len,"%d\n",dev->nums[index]);
    }

    if(p >= len){
        return 0;
    }
    if(count > len - p){
		count = len - p;
    }
    if(copy_to_user(buf,(void*)(tbuf),count)){
        return -EINVAL;
    }else{
        *ppos += count;
        ret = count;
    }
    return ret;
}

static ssize_t chr_write(struct file* filp, const char __user* buf, size_t size, loff_t* ppos){
    unsigned long p = *ppos;
    unsigned int count = size;
    int ret = 0;
    /*int index = 0;*/
    struct chr_dev* dev = filp->private_data;
    char tbuf[CHRMEM_SIZE];
    memset(tbuf, 0, CHRMEM_SIZE);

    if(p >= CHRMEM_SIZE){
        return 0;
    }
    if(count > CHRMEM_SIZE - p){
        count = CHRMEM_SIZE - p;
    }
    if(copy_from_user(tbuf,buf,count)){
        ret = -EINVAL;
    }else{
        loff_t l = strlen(tbuf);
        memcpy(dev->mem+p,tbuf,l);
		dev->nums[num_pos] = calnum(tbuf,l);
		num_pos++;
        *ppos += l;
        ret = l;
        printk(KERN_DEBUG "ppos %lld\n",*ppos);
    }
    return ret;
}

static loff_t chr_llseek(struct file* filp, loff_t offset, int orig){
    loff_t ret = 0;
    switch (orig) {
        case 0:
            if(offset < 0){
                ret = -EINVAL;
                break;
            }
            if((unsigned int)offset > CHRMEM_SIZE){
                ret = -EINVAL;
                break;
            }
            filp->f_pos = (unsigned int)offset;
            ret = filp->f_pos;
            break;
        case 1:
            if((filp->f_pos+offset)>CHRMEM_SIZE){
                ret = -EINVAL;
                break;
            }
            if( filp->f_pos+offset < 0){
                ret = -EINVAL;
                break;
            }
            filp->f_pos += offset;
            ret = filp->f_pos;
            break;
        default:
            ret = -EINVAL;
            break;
    }
    return ret;
}

static const struct file_operations chr_ops = 
{
    .owner    = THIS_MODULE,
    .llseek   = chr_llseek,
    .read     = chr_read,
    .write    = chr_write,
    .open     = chr_open,
    .release  = chr_release
};


static void chr_setup_cdev(struct chr_dev* dev, int index){
    int err;
    int devno = MKDEV(chr_major,index);
    cdev_init(&dev->cdev,&chr_ops);
    dev->cdev.owner = THIS_MODULE;
    err = cdev_add(&dev->cdev,devno,1);
    if(err){
        printk(KERN_NOTICE "Error happend!\n");
    }
}

int chr_init(void){
    int result;
    dev_t ndev;
    result = alloc_chrdev_region(&ndev,0,1,"var3");
    if(result < 0){
        return result;
    }
    printk("chr_init(): major = %d, minor = %d\n", MAJOR(ndev), MINOR(ndev));
    chr_major = MAJOR(ndev);
    char_devp = kmalloc(sizeof(struct chr_dev),GFP_KERNEL);
    num_pos = 0;
    if(!char_devp){
        result = -ENOMEM;
        goto err1;
    }
    memset(char_devp,0,sizeof(struct chr_dev));
    chr_setup_cdev(char_devp,0);
    return 0;
err1:
    unregister_chrdev_region(ndev,1);
    return 0;
}

void chr_exit(void){
    cdev_del(&char_devp->cdev);
    kfree(char_devp);
    unregister_chrdev_region(MKDEV(chr_major,0),1);
}

static int proc_show(struct seq_file *seq, void *v)
{
    int index;
    for(index=0;index<num_pos;index++){
        seq_printf(seq,"%d\n",char_devp->nums[index]);
    }
    return 0;
}

int proc_open(struct inode* inode, struct file* filp){

    single_open(filp, proc_show, NULL);
    return 0;
}

static ssize_t proc_write(struct file *file, const char __user * ubuf, size_t count, loff_t* ppos) 
{
    unsigned long p = *ppos;
    /*unsigned int count = size;*/
    int ret = 0;
    struct chr_dev* dev = char_devp;
    char* tbuf;

    if(p > CHRMEM_SIZE){
        printk(KERN_NOTICE "over size %d",p);
        return 0;
    }else if(p == CHRMEM_SIZE){
        printk(KERN_NOTICE "trig size %d",p);
	return count ? -ENXIO : 0;
    }
    if(count > CHRMEM_SIZE - p){
        count = CHRMEM_SIZE - p;
    }
    tbuf = kmalloc(CHRMEM_SIZE,GFP_KERNEL);
    memset(tbuf, 0, CHRMEM_SIZE);
    if(copy_from_user(tbuf,ubuf,count)){
        ret = -EINVAL;
    }else{
        loff_t l = strlen(tbuf);
        memcpy(dev->mem+p,tbuf,l);
		dev->nums[num_pos] = calnum(tbuf,l);
		num_pos++;
        *ppos += l;
        ret = l;
    }
    kfree(tbuf);
    return ret;
}


static struct file_operations fops = {
	.owner = THIS_MODULE,
   	.open = proc_open,
	.read = seq_read,
	.write = proc_write,
   	.llseek = seq_lseek,
	.release = single_release,
};

static int __init proc_example_init(void)
{
    chr_init();
    entry = proc_create("var3", 0660, NULL, &fops);
    if(!entry){
	    printk(KERN_INFO "%s: proc file is created failed\n", THIS_MODULE->name);
    }
	printk(KERN_INFO "%s: proc file is created\n", THIS_MODULE->name);
	return 0;
}

static void __exit proc_example_exit(void)
{
	proc_remove(entry);
    chr_exit();
	printk(KERN_INFO "%s: proc file is deleted\n", THIS_MODULE->name);
}


module_init(proc_example_init);
module_exit(proc_example_exit);

MODULE_LICENSE("WTFPL");
MODULE_AUTHOR("Dmitrii Medvedev");
MODULE_DESCRIPTION("A simple example Linux module.");
MODULE_VERSION("0.1");
